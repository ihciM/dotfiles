vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.compatible = false
vim.opt.termguicolors = true
vim.opt.showmode = false
vim.opt.cursorline = true
vim.g.mapleader = " "

vim.cmd("colorscheme northern-lights")
